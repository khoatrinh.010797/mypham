// var swiper = new Swiper('.swiper-main-container', {
//     pagination: '.swiper-pagination',
//     paginationClickable: true,
//     calculateHeight: true,
//     autoplay: 3000,
//     speed: 700,
//     effect: 'fade'
// });
// var swiper_topic = new Swiper('.swiper-topic', {
//     effect: 'coverflow',
//     grabCursor: true,
//     centeredSlides: false,
//     loop: true,
//     slidesPerView: 'auto',
//     coverflowEffect: {rotate: 50, stretch: 0, depth: 100, modifier: 1, slideShadows: true,},
//     pagination: {el: '.swiper-pagination',},
// });
// var swiper_quote = new Swiper('.swiper-quote-container', {
//     pagination: '.swiper-pagination',
//     paginationClickable: true,
//     nextButton: '.swiper-button-next',
//     prevButton: '.swiper-button-prev',
//     parallax: true,
//     speed: 600,
// });
// var swiper_product = new Swiper('.swiper-product-container', {
//     pagination: '.swiper-pagination',
//     slidesPerView: 4,
//     slidesPerGroup: 4,
//     paginationClickable: true,
//     watchOverflow: true,
//     spaceBetween: 5,
//     breakpoints: {
//         480: {
//             slidesPerView: 2
//         }
//     },
//     onInit: function (swiper) {
//         if (swiper.slides.length === 1) {
//             $('.swiper-pagination').hide();
//         }
//     }
// });
var swiper_patner = new Swiper('.swiper-partner-container', {
    slidesPerView: 5,
    spaceBetween: 30,
    slidesPerGroup: 1,

    loop: true,
    loopFillGroupWithBlank: true,
    autoplay: true,
    pagination: {
        // el: '.swiper-pagination-product-discount',
        clickable: true,
    },
});

var swiper_patner_mobile = new Swiper('.swiper-partner-container-mobile', {
    slidesPerView: 2,
    spaceBetween: 30,
    slidesPerGroup: 1,

    loop: true,
    loopFillGroupWithBlank: true,
    autoplay: true,
    pagination: {
        // el: '.swiper-pagination-product-discount',
        clickable: true,
    },
});
var swiper = new Swiper('.swiper-container-not-children', {
    slidesPerView: 1,
    slidesPerGroup: 1,
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var sw;
if (document.getElementsByClassName('swiper-main-container').length > 0 && Swiper) {
    sw = new Swiper('.swiper-main-container', {
        loop: true,
        autoplay: true,
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        // slidesPerView: 4
    });
}

/* plugin  */
$(function () {

    $(".div_cate_vote").rateYo({
        rating: $(".div_cate_vote").data('rate-init'),
        fullStar: true,
        starWidth: "15px"
    }).on("rateyo.set", function (e, data) {
        rate_vote = data.rating;
        object_id = $(".div_cate_vote").data('object_id');
        _data = 'object_id='+object_id+'&vote_rate='+rate_vote ;
        $.ajax({
            url:  $(".div_cate_vote").data('url-post'),
            data: _data,
            type: 'POST',
            datatype: '',
            catch:false,
            success: function(data){
                result = JSON.parse(data);
            }
        });
    });
});

(function ($) {
    $.fn.stickyNav = function (opt) {
        var o = this, w = $(window), options = $.extend(opt, {});
        var threshold = options.threshold || 120;
        var clsName = options.clsName || 'in';

        var _runner = function (pos) {
            if (pos > threshold) {
                o.addClass(clsName);
            } else {
                o.removeClass(clsName);
            }
        };

        _runner(w.scrollTop());

        w.scroll(function () {
            _runner(w.scrollTop());
        });

        return o;
    };
    var nav = $('.navbar').stickyNav({
        threshold: 66,
        clsName: 'slide-down'
    });

    $.fn.responsiveImage = function () {
        var o, img, src, _runner = function () {
            o = $(this);
            if ((img = $('img', o)) && (src = img.attr('src')) !== null) {
                o.css('background-image', 'url(\'' + src + '\')').css('background-size', 'cover');
            }
        };
        if (!this.hasClass('img-wrap')) {
            return $('.img-wrap', this).each(_runner);
        }
        return this.each(_runner);
    };

    $('.project-tab[data-toggle="tab"]:visible').on('shown.bs.tab', function (e) {
        var categoryId = $(e.target).data('index');
        $.get('/project/ajaxCategory/' + categoryId, function (res) {
            $('.tab-content').html(res).responsiveImage();
        });
    });

    var firstCat;
    if ((firstCat = $('.category-nav:visible > li.active:first-child > a')) && firstCat.length > 0) {
        $.get('/project/ajaxCategory/' + firstCat.data('index'), function (res) {
            $('.tab-content').html(res).responsiveImage();
        });
    }

    $('.category-top-tab[data-toggle="tab"]:visible').on('shown.bs.tab', function (e) {
        var categoryId = $(e.target).data('index');
        $.get('/category/ajaxCategoryTop/' + categoryId, function (res) {
            $('.tab-content').html(res).responsiveImage();
        });
    });

    var firstCatTop;
    if ((firstCatTop = $('.category-top-nav:visible > li.active:first-child > a')) && firstCatTop.length > 0) {
        $.get('/category/ajaxCategoryTop/' + firstCatTop.data('index'), function (res) {
            $('.tab-content').html(res).responsiveImage();
        });
    }

    var getCartInfo = function () {
        let cart, badge = $('#cart-badge');
        $.get('/cart', (res) => {
            if (!res || !(cart = JSON.parse(res))) return;
            $('span', badge).text(cart.total);
        });
    };
    $(document).ready(getCartInfo);
    var o;
    $('.add-to-cart-btn:visible').each(function () {
        o = $(this);
        o.click(function () {
            $.post(o.attr('href'), o.data(), function (res) {
                if (res) {
                    $.toast({
                        heading: 'Giỏ hàng',
                        text: 'Thêm vào giỏ hàng thành công',
                        icon: 'success',
                        bgColor: 'green',
                        position: 'bottom-right'
                    });
                    getCartInfo();
                }
            });
            return false;
        });
    });

    $('.img-wrap:visible').responsiveImage();

    $('.e-carousel:visible').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        thumbItem: 4,
        slideMargin: 0,
        enableDrag: true,
        controls: true,
        currentPagerPosition: 'left',
        onSliderLoad: function (el) {
            $(el).removeClass('no-js');
            $('.lSPager.lSGallery > li > a', o).addClass('img-wrap').responsiveImage();
        }
    });

    $('.content-table:visible').each(function () {
        var o = $(this);
        $('a.btn-tb-content:visible', o).on('click', function (e) {
            e.preventDefault();
            o.toggleClass('closed');
        });
    });

    $('.gallery-wrap:visible').each(function () {
        var o = $(this);
        $('.singer-carousel:visible', o).lightSlider({
            gallery: true,
            item: 1,
            loop: true,
            thumbItem: 4,
            slideMargin: 0,
            enableDrag: true,
            controls: true,
            currentPagerPosition: 'left',
            onSliderLoad: function (el) {
                $(el).removeClass('no-js');
                $('.lSPager.lSGallery > li > a', o).addClass('img-wrap').responsiveImage();
            }
        });
    });

    $('.video-modal').on('shown.bs.modal', function (e) {
        var modal = $(this), btn = $(e.relatedTarget), src = btn.data('src');
        $('.modal-content', modal).html('<div class="embed-responsive embed-responsive-4by3">' +
            '<iframe src="' + src + '" frameborder="0" ' +
            'allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ' +
            'allowfullscreen class="embed-responsive-item"></iframe>' +
            '</div>');
    }).on('hidden.bs.modal', function (e) {
        $('.modal-content', $(this)).html('');
    });

    $('#contactConfirmModal').on('hidden.bs.modal', function () {
        console.log('hide contactConfirmModal and post');
        // do something…
        form = $("#contactFormAjax");
        //$("#contactFormAjax").ajaxSubmit({url: form.attr('action'), type: 'post'});
        $.post(form.attr('action'), $('#contactFormAjax').serialize());
        $('#contactFormAjax').trigger("reset");
        grecaptcha.reset();
    });

    $( "#contactFormAjax" ).submit(function( event ) {
        var response = grecaptcha.getResponse();

        if(response.length == 0){
            return false;
        }else {
            $('#contactModal').modal('hide');
            //alert( "Handler for .submit() called." );
            $('#contactConfirmModal').modal();
        }

        //event.preventDefault();
        return false;
    });


})(jQuery);

function add_view_post(id){
    var base_url = window.location.origin;
    var url = base_url + '/Category/add_view_post/';
    var  post_id = {id:id} ;
    $.ajax({
        url: url ,
        data: post_id,
        type: 'POST',
        datatype: '',
        catch:false,
        success: function(data){
            // result = JSON.parse(data);
            // console.log();
        }
    });
}
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("fixed_menu");
        $('.navbar-inverse').addClass('fix-main-nav');
        $('.img-responsive').addClass('fix-image-logo');
    } else {
        header.classList.remove("fixed_menu");
        $('.navbar-inverse').removeClass('fix-main-nav');
        $('.img-responsive').removeClass('fix-image-logo');
    }
}