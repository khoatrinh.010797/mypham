<?php

use App\Helpers\Html;
use App\Helpers\BaseArrayHelper;

?>
<section class=col-md-12>
    <div class=tabProduct>
        <div class=row>
            <div class="homepage-row-product-title text-center">
                <h2>
                    <a href="/thung-carton" title="Thùng carton">Thùng carton</a>
                    <hr class="underline-title">
                </h2>
            </div>
        </div>
        <div>
            <div class="hidden-xs">
                <div class="row">
                        <div class="swiper-container  swiper-container-not-children">
                            <div class="swiper-wrapper">
                                <?php

                                if ($models && !empty($models)) {
                                    $page = 1;
                                    $page_size = 4;
                                    $sub_cate = BaseArrayHelper::get_page($models, $page, $page_size);
                                    while (count($sub_cate)) {
                                        ?>


                                        <div class="swiper-slide">
                                            <div>
                                                <div class="row">
                                                    <?php foreach ($sub_cate as  $category) {
                                                        ?>

                                                        <div class="col-md-6 img-wrap-not-children" style="margin-bottom: 10px">
                                                            <a href="<?= $category->getUrl() ?>" class="hover14"
                                                               title="<?= Html::decode($category->title) ?>">
                                                                <figure class="img-wrap square "
                                                                        style="padding-top: 49%; border: solid 1px #8b1409;">
                                                                    <?= Html::img($category->getImage(), ['alt' => $category->title]) ?>
                                                                </figure>
                                                                <div class="tile-not-chilren">
                                                                    <h3><?= Html::decode($category->title) ?></h3>
                                                                </div>
                                                                <div class="not-children">
                                                                    <a href="<?= $category->getUrl() ?>">
                                                                        <button type="button" class="btn btn-danger">XEM THÊM
                                                                        </button>
                                                                    </a>
                                                                </div>

                                                            </a>
                                                        </div>


                                                        <?php
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>


                                        <?php
                                        $page = $page + 1;
                                        $sub_cate = BaseArrayHelper::get_page($models, $page, $page_size);
                                    }
                                }
                                ?>

                            </div>
                            <!-- Add Pagination -->
                            <br>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>



                </div>

            </div>

            <div class="visible-xs">

                <div class="row">
                    <?php foreach ($models as $category): ?>
                        <div class="col-xs-6 col-md-6">
                            <a href="<?= $category->getUrl() ?>" class="thumbnail product-box"
                               title="<?= Html::decode($category->title) ?>">
                                    <span class="img-wrap">
                                        <?= Html::img($category->getImage(), ['alt' => $category->title]) ?>
                                    </span>
                                <div class="caption" style="text-align: center; height: 70px"><span
                                            style="line-height: 35px"><?= Html::decode($category->title) ?></span></div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
</section>
