<?php

use App\Helpers\Html;
use App\Helpers\StringHelper;
use App\Helpers\Widgets\NewsKingNghiemHay;
use App\Models\SettingsModel;
use App\Helpers\SettingHelper;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\SliderModel[] $sliders
 * @var \App\Models\ProjectCategoryModel[] $projectCategories
 * @var \App\Models\CategoryModel[] $categories
 * @var \App\Models\TestimonialModel[] $testimonials
 * @var \App\Models\PartnerModel[] $partners
 * @var \App\Models\NewsModel[] $newsItems
 */
$this->title = $title;
$this->meta_image_url = $meta_image_url;
$home_posts_block_id = explode(',', $settings['home_posts_block_id']);
?>
<div class="swiper-container swiper-main-container">
    <?php if ($sliders && !empty($sliders)): ?>
        <div class=swiper-wrapper>
            <?php foreach ($sliders as $slider): ?>
                <?= Html::beginTag('a', ['title' => $slider->title, 'href' => $slider->url, 'target' => '_blank', 'class' => 'swiper-slide']); ?>
                <?= Html::img($slider->getImage(), ['alt' => $slider->title, 'style' => ['width' => '100%']]); ?>
                <?= Html::endTag('a'); ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class=swiper-pagination></div>
</div>
<div class="container">
    <section class="section_1">
    <div class="row block_1">
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 block_2">
						<!-- <img src="https://maihan.vn/template/maihan/image/index_1.jpg" alt="Hoan tien" title="Hoàn tiền 200%" class="img-responsive img_1 lazy"> -->
						<svg xmlns="http://www.w3.org/2000/svg" width="52px" height="51px" viewBox="0 0 52 51" version="1.1">
						<g id="#8bc53fff">
						<path fill="#8bc53f" opacity="1.00" d=" M 21.61 0.00 L 31.07 0.00 C 35.81 1.55 40.23 3.90 44.24 6.86 C 44.94 5.75 45.74 4.70 46.68 3.78 C 46.84 6.31 46.79 8.84 46.59 11.36 C 42.59 8.70 39.37 4.81 34.66 3.32 C 23.41 -1.15 9.41 4.22 4.22 15.19 C 0.68 22.21 1.08 31.02 5.25 37.69 C 9.56 44.78 17.70 49.31 26.00 49.23 C 34.55 49.33 42.94 44.52 47.16 37.08 C 49.30 33.45 49.54 29.01 52.00 25.53 L 52.00 29.26 C 50.72 35.01 48.00 40.57 43.47 44.44 C 39.81 47.81 35.07 49.58 30.39 51.00 L 21.67 51.00 C 15.35 49.28 9.14 46.19 5.18 40.81 C 2.37 37.26 0.87 32.89 0.00 28.50 L 0.00 21.61 C 1.16 17.59 2.55 13.52 5.18 10.19 C 9.13 4.82 15.32 1.75 21.61 0.00 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 13.49 16.25 C 22.54 15.74 31.66 15.83 40.72 16.21 C 41.01 21.47 40.97 26.75 40.74 32.01 C 35.52 32.65 30.24 32.17 25.00 32.31 C 21.15 32.19 17.24 32.71 13.45 31.88 C 13.32 26.67 13.26 21.46 13.49 16.25 M 25.29 20.06 C 22.57 21.15 21.67 25.12 23.86 27.16 C 26.17 29.80 31.04 28.40 31.57 24.92 C 32.37 21.52 28.35 18.62 25.29 20.06 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 12.91 34.79 C 13.81 32.79 16.21 33.55 17.94 33.36 C 25.46 33.48 32.99 33.22 40.51 33.47 C 40.62 33.84 40.85 34.57 40.97 34.94 C 31.62 35.07 22.24 35.31 12.91 34.79 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 13.00 36.21 C 22.27 35.68 31.58 35.92 40.85 36.08 C 40.81 36.46 40.71 37.24 40.67 37.62 C 31.72 37.77 22.71 38.00 13.78 37.52 C 13.58 37.19 13.20 36.54 13.00 36.21 Z"></path>
						</g>
						</svg>
						<p class="text_1"><b>Hoàn tiền 200%</b> nếu phát hiện bán hàng nhái giả</p>
						
					</div>
					<div class="border_left"></div>
					

					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 block_2">
						<!-- <img src="https://maihan.vn/template/maihan/image/index_2.jpg" alt="Doi tra" title="Đổi trả" class="img-responsive img_1 lazy"> -->
						<svg xmlns="http://www.w3.org/2000/svg" width="55px" height="51px" viewBox="0 0 55 51" version="1.1">
						<g id="#8bc53fff">
						<path fill="#8bc53f" opacity="1.00" d=" M 23.62 0.00 L 31.38 0.00 C 39.87 1.50 47.79 7.24 50.34 15.71 C 50.95 18.51 52.80 22.06 50.60 24.56 C 49.55 19.43 48.78 13.92 45.25 9.79 C 37.15 -0.66 19.60 -1.00 10.92 8.91 C 6.52 13.37 5.29 19.71 4.46 25.68 C 2.86 24.02 1.27 22.32 0.00 20.39 L 0.00 19.19 C 1.11 19.97 2.24 20.73 3.36 21.51 C 4.22 17.43 5.39 13.30 7.92 9.91 C 11.55 4.61 17.51 1.52 23.62 0.00 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 13.60 17.53 C 17.28 15.38 21.12 13.50 24.96 11.65 C 28.84 13.57 32.80 15.36 36.57 17.51 C 32.88 19.73 28.94 21.49 25.08 23.40 C 21.25 21.44 17.27 19.78 13.60 17.53 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 13.20 18.33 C 17.12 19.98 20.91 21.92 24.73 23.79 C 24.75 28.52 24.83 33.26 24.64 37.99 C 20.72 36.12 16.92 34.01 13.12 31.88 C 13.14 27.36 13.06 22.84 13.20 18.33 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 25.39 23.80 C 29.22 21.93 33.00 19.95 36.94 18.32 C 36.99 20.43 37.00 22.53 37.00 24.64 C 34.92 25.50 32.61 26.34 31.34 28.35 C 29.78 30.51 30.04 33.30 29.91 35.83 C 28.44 36.54 26.97 37.27 25.51 37.99 C 25.28 33.27 25.38 28.53 25.39 23.80 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 35.33 26.25 C 40.25 23.88 46.17 28.72 45.28 33.99 C 45.04 39.08 38.71 42.46 34.43 39.57 C 29.40 36.73 29.91 28.35 35.33 26.25 M 34.73 29.28 C 35.92 29.94 37.15 30.55 38.39 31.12 C 37.64 33.21 37.10 35.37 36.84 37.57 C 39.60 35.77 39.54 31.94 41.06 29.22 C 38.95 29.20 36.84 29.20 34.73 29.28 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 50.54 25.33 C 52.15 26.99 53.73 28.68 55.00 30.62 L 55.00 31.82 C 53.88 31.03 52.76 30.27 51.64 29.49 C 50.57 35.70 47.79 41.81 42.70 45.72 C 39.45 48.44 35.39 49.85 31.38 51.00 L 23.62 51.00 C 15.13 49.49 7.21 43.76 4.66 35.29 C 4.02 32.48 2.25 28.98 4.37 26.42 C 5.46 31.55 6.21 37.07 9.75 41.21 C 17.85 51.66 35.39 52.00 44.07 42.09 C 48.48 37.63 49.71 31.30 50.54 25.33 Z"></path>
						</g>
						</svg>
						<p class="text_1"><b>Đổi trả</b> trong vòng 7 ngày</p>
						
					</div>

					<div class="clear"></div>

					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 block_2">
						<div class="border_bottom"></div>
						<!-- <img src="https://maihan.vn/template/maihan/image/index_3.jpg" alt="Giam them" title="Giảm thêm 4%"  class="img-responsive img_1 lazy"> -->
						<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="51px" height="51px" viewBox="0 0 51.000000 51.000000" preserveAspectRatio="xMidYMid meet">  <g transform="translate(0.000000,51.000000) scale(0.050000,-0.050000)" fill="#8bc43f" stroke="none"> <path d="M290 972 c-492 -232 -324 -972 220 -972 292 0 509 217 509 510 1 379 -386 624 -729 462z m432 -33 c195 -99 309 -334 259 -532 -103 -410 -612 -513 -860 -174 -292 397 162 930 601 706z"></path> <path d="M200 685 c0 -32 28 -35 300 -35 272 0 300 3 300 35 0 32 -28 35 -300 35 -272 0 -300 -3 -300 -35z"></path> <path d="M200 440 l0 -160 300 0 300 0 0 160 0 160 -300 0 -300 0 0 -160z m235 125 c-42 -4 -105 -4 -140 1 -36 4 -2 8 75 7 77 0 106 -3 65 -8z"></path> </g> </svg> 
						<p class="text_1"><b>Giảm thêm 4%</b> khi chuyển khoản trước</p>

					</div>
					<div class="border_left"></div>
					

					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 block_2 hide_desktop">
						<div class="border_bottom"></div>
						<!-- <img src="https://maihan.vn/template/maihan/image/index_4.jpg" alt="Freeship" title="Freeship" class="img-responsive img_2 lazy"> -->
						<svg xmlns="http://www.w3.org/2000/svg" width="52px" height="26px" viewBox="0 0 52 26" version="1.1" class="img_2">
						<g id="#8bc53fff">
						<path fill="#8bc53f" opacity="1.00" d=" M 10.69 0.00 L 38.76 0.00 C 38.73 7.53 38.93 15.04 39.32 22.56 C 34.13 22.67 28.94 22.60 23.76 22.65 C 22.20 20.99 20.59 18.77 17.97 19.21 C 15.65 18.47 15.17 21.46 14.18 22.77 C 13.31 22.70 11.58 22.57 10.72 22.50 C 10.68 18.28 10.68 14.06 10.71 9.85 C 8.10 9.92 5.48 9.97 2.89 9.68 C 5.44 9.08 8.06 8.98 10.67 8.82 C 10.72 8.28 10.83 7.21 10.88 6.67 C 8.06 6.65 5.23 6.57 2.43 6.19 C 5.18 5.74 7.98 5.66 10.76 5.60 C 10.72 3.74 10.70 1.87 10.69 0.00 M 11.82 6.00 C 12.30 7.22 16.14 7.28 16.59 5.98 C 15.16 5.27 13.26 5.33 11.82 6.00 M 11.46 9.74 C 12.13 10.63 14.59 10.67 14.50 9.24 C 13.82 8.37 11.37 8.31 11.46 9.74 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 39.52 4.45 C 41.93 4.57 44.40 4.40 46.77 4.99 C 49.20 7.51 50.49 10.88 52.00 13.99 L 52.00 21.36 C 51.40 21.77 50.21 22.59 49.62 23.00 C 48.69 23.99 47.79 25.01 46.87 26.00 L 44.06 26.00 C 43.05 24.81 42.14 23.55 41.23 22.29 C 40.88 22.43 40.18 22.70 39.83 22.83 C 39.54 16.71 39.43 10.58 39.52 4.45 M 41.32 6.09 C 41.21 7.84 41.15 9.59 41.12 11.33 C 43.66 11.44 46.21 11.44 48.76 11.31 C 47.75 9.68 46.87 7.94 45.57 6.51 C 44.17 6.19 42.73 6.20 41.32 6.09 M 45.17 21.35 C 43.54 21.68 44.28 24.32 45.85 23.80 C 47.47 23.46 46.76 20.82 45.17 21.35 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 0.00 9.25 C 0.69 9.22 2.08 9.16 2.78 9.13 C 2.19 10.30 1.26 10.56 0.00 9.92 L 0.00 9.25 Z"></path>
						<path fill="#8bc53f" opacity="1.00" d=" M 16.49 20.48 C 18.50 18.22 22.27 19.98 22.65 22.74 C 22.12 23.86 21.53 24.95 20.87 26.00 L 17.25 26.00 C 15.96 24.41 14.29 22.10 16.49 20.48 M 18.07 21.50 C 16.46 22.55 18.44 24.79 19.82 23.68 C 21.47 22.67 19.44 20.39 18.07 21.50 Z"></path>
						</g>
						</svg>
						<p class="text_1"><b>Freeship</b> cho đơn hàng trị giá trên 500k</p>
					</div>
				</div>
    </section>
    <section class="grid-box" style="margin-top:0px" >
        <?php
        if ($home_posts_block_id && count($home_posts_block_id) > 1):
            foreach ($home_posts_block_id as $block_id):
                if ($postsCategories && !empty($postsCategories)):
                    ?>
                    <div class="grid-box__content clearfix pd10">
                        <?php foreach ($postsCategories as $posts_category):
                            if ($posts_category->id == $block_id) :
                                ?>
                                <div class="head-tab">
                                    <h2 class="section-title">
                                        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                                             fill-rule="evenodd"
                                             clip-rule="evenodd">
                                            <path d="M11.5 23l-8.5-4.535v-3.953l5.4 3.122 3.1-3.406v8.772zm1-.001v-8.806l3.162 3.343 5.338-2.958v3.887l-8.5 4.534zm-10.339-10.125l-2.161-1.244 3-3.302-3-2.823 8.718-4.505 3.215 2.385 3.325-2.385 8.742 4.561-2.995 2.771 2.995 3.443-2.242 1.241v-.001l-5.903 3.27-3.348-3.541 7.416-3.962-7.922-4.372-7.923 4.372 7.422 3.937v.024l-3.297 3.622-5.203-3.008-.16-.092-.679-.393v.002z"></path>
                                        </svg>
                                        <a href="<?= $posts_category->getUrl() ?>"
                                           title="<?= $posts_category->title ?>"><?= $posts_category->title ?></a>
                                        <i class="fas fa-caret-right"></i>
                                    </h2>
                                    <!-- <?php if ($categories_menu):
                                        $menu = $categories_menu->getCategoryRecursive($posts_category->id, 0, 2)
                                        ?>
                                        <ul class="nav nav-tabs hidden-xs hidden-sm">
                                            <?php foreach ($menu as $mn): ?>
                                                <li class="cat-item cat-item-23"><a
                                                            href="<?= $mn->getUrl() ?>"
                                                            title=" <?= $mn->title ?>"><?= $mn->title ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                    <a class="load-cat text-danger" href="<?= $posts_category->getUrl() ?>">Xem tất cả</a> -->
                                </div>
                                <div class="box-content__inner">
                                    <div class="row no-gutters5">
                                        <?php
                                        if ($posts_category->posts_hot) {
                                            foreach ($posts_category->posts_hot as $posts_hot):?>
                                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                                                    <div class="card-product"
                                                         onclick="add_view_post(<?= $posts_hot->id ?>)">
                                                        <div class="card-image">
                                                            <a href="<?= $posts_hot->getUrl() ?>">
                                                                <img width="276" height="276"
                                                                     src="<?= $posts_hot->getImage() ?>"
                                                                     class="img-responsive center-block wp-post-image"
                                                                     alt="Combo 20 hộp carton đựng giày MS: HG2-size: 28x16x12 cm"
                                                                     srcset="<?= $posts_hot->getImage() ?>"
                                                                     sizes="(max-width: 276px) 100vw, 276px"> </a>
                                                        </div>
                                                        <div class="card-content">
                                                            <h4 class="card-title"><a
                                                                        href="<?= $posts_hot->getUrl() ?>"><?= $posts_hot->title ?></a></h4>
                                                            <a class="cart-btn" href="<?= $posts_hot->getUrl() ?>">Xem Ngay</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach;
                                        } ?>
                                    </div>
                                    <div class="box-content__inner" style="text-align: center;">
					                   <a href="<?= $posts_category->getUrl() ?>" title="show more"><b href="#" class="btn btn-default" id="xemthem">Xem thêm</b></a>
				                   </div>
                                </div>
                            <?php
                            endif;
                        endforeach; ?>
                    </div>
                <?php endif;
            endforeach;
        endif;
        ?>
    </section>
</div>
<div class="embed-responsive embed-responsive-video">
    <?php echo $settings['home_master_video']; ?>

    <!-- Replace with more video from youtube here! -->
</div>
<div class=container>
    <div class=row>
        <section class=col-md-12>
            <div class="homepage-row-product-title text-center"><h2></h2></div>
            <div class=col-md-6>
                <div class=homeContent><p>
                    <ul style=margin-left:5px>
                        <?php if ($homeContent) { ?>
                            <?php foreach ($homeContent as $home_contents) { ?>
                                <li><span style=font-size:12pt><?= $home_contents->title ?></span></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class=col-md-6>
                <img src="<?php echo SettingHelper::getSettingImage($settings['home_banner_2']); ?>" alt="home_banner_2"
                     class=img-responsive>
            </div>
        </section>
    </div>
    <?php if ($testimonials && !empty($testimonials)): ?>
        <div class=container>
            <div class=row>
                <section class=col-md-12 style=display:flex;flex-wrap:wrap>
                    <?php foreach ($testimonials as $item): ?>
                        <div class="col-xs-6 col-md-3">
                            <div>
                                <div class="img-wrap square circle">
                                    <?= Html::img($item->getImage(), ['alt' => $item->full_name]) ?>
                                </div>
                                <h3 class="csname text-center"><?= Html::decode($item->full_name) ?></h3>
                                <p><?= $item->intro && !empty($item->intro) ? StringHelper::truncateWords(
                                        $item->intro, 200) : null ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </section>
            </div>
        </div>
    <?php endif; ?>
    <div class=row>
        <section class=col-md-12>
            <?= NewsKingNghiemHay::register($this) ?>
        </section>
    </div>
</div>

<section>
    <div class="container">
        <div class="row">
            <?php for ($i = 1; $i <= 3; $i++): ?>
                <div class="col-md-4 col-lg-4 col-xs-4">
                    <button type="button" data-toggle="modal" data-target="#video-<?= $i ?>"
                            data-src="<?php echo $settings['home_video_link_' . $i]; ?>"
                            class="img-wrap anim player">
                        <img src="<?php echo SettingHelper::getSettingImage($settings['home_video_thumb_' . $i]); ?>"
                             alt="">
                    </button>
                    <div class="modal fade video-modal" id="video-<?= $i ?>" tabindex="-1" role="dialog"
                         aria-labelledby="video-<?= $i ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                #Video <?= $i ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</section>

<div class="container">
    <?php if ($partners && !empty($partners)): ?>
        <div class=row>
            <section class=col-md-12>
                <div class=row>
                    <div class="homepage-row-product-title text-center">
                        <h2>
                            <a>BÁO CHÍ NÓI GÌ VỀ CHÚNG TÔI</a>
                            <hr class="underline-title">
                        </h2>

                    </div>
                </div>
                <div class="swiper-container swiper-partner-container hidden-xs">
                    <div class="swiper-wrapper">
                        <?php foreach ($partners as $partner): ?>
                            <div class="swiper-slide">
                                <div class="text-center">
                                    <a rel="nofollow" href="<?= $partner->getUrl() ?>" target=_blank
                                       title="<?= Html::decode($partner->title) ?>">
                                        <?= Html::img($partner->getImage(), ['alt' => $partner->title, 'style' => ['height' => '80px']]) ?>
                                    </a>
                                </div>

                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="swiper-partner-container"></div>
                </div>
                <div class="swiper-container swiper-partner-container-mobile visible-xs">
                    <div class="swiper-wrapper">
                        <?php foreach ($partners as $partner): ?>
                            <div class="swiper-slide">
                                <div class="text-center">
                                    <a rel="nofollow" href="<?= $partner->getUrl() ?>" target=_blank
                                       title="<?= Html::decode($partner->title) ?>">
                                        <?= Html::img($partner->getImage(), ['alt' => $partner->title, 'style' => ['width' => '80%']]) ?>
                                    </a>
                                </div>

                            </div>
                        <?php endforeach; ?>

                    </div>
                    <div class="swiper-partner-container"></div>
                </div>
            </section>
        </div>
    <?php endif ?>
</div>