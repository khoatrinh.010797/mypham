<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\CategoryModel[] $items
 * @var \App\Models\ProjectCategoryModel[] $projects
 */
?>
<div class="top_head hidden-xs">
    <div class="container-fluid block_1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-md-push-7 text-right">
                    <a href="/dai-ly/" title="Chính sách Đại Lý">Chính sách đại lý</a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-md-push-7 text-right">
                    <a href="/gioi-thieu/" title="Về Mai Hân">Giới thiệu</a>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-md-push-7 text-right">
                    <a href="/lien-he/" title="Liên Hệ">Liên hệ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="header" id="myHeader">
        <div class=container>
            <div class=row>
                <div class="col-xs-12 col-md-2 search-bar">
                    <div class="logo edit_potion_logo">
                        <a href="/" title="Về trang chủ">
                            <img src=<?php echo base_url('/images/'.$settings['home_logo_link'])?> alt="" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 header-right text-right">
                    <div class="search-bar hidden-xs">
                        <form action="<?= route_to('home_search') ?>" method=get>
                            <div id=custom-search-input>
                                <div class=input-group>
                                    <input type=text name=query class="form-control input-sm"
                                           placeholder="Nhập từ khóa để tìm kiếm...">
                                    <span class=input-group-btn>
                                    <button class="btn btn-info btn_search" type=submit>
                                        <img src="/images/search.svg">
                                    </button>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-5 ">
                    <div class="tieude" style="margin-top: 20px; transition: all 0s ease 0s;">
                        <div class="block_1">
                            <img src="https://maihan.vn/template/maihan/img/icon/ship.svg" alt="Free ship" title="Miễn phí vận chuyển" class="lazy">
                            <p>Freeship cho đơn hàng<br> 500k trở lên</p>
                        </div>
                        <div class="block_2"><a href="tel:19002059" title="">
                                <img src="https://maihan.vn/template/maihan/img/icon/phone.svg" alt="Free ship" title="Miễn phí vận chuyển" class="lazy">
                                <p><b>Hotline (24/7)</b> <br><b class="red">1900 2059</b></p></a>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-inverse main-nav">
                <div class=container>
                    <div class=navbar-header>
                        <a href="/"><img class="visible-xs" src="<?php echo base_url('/images/' . $settings['home_logo_link']) ?> " alt=""></a>
                        <button type="button" class="navbar-toggle" data-target="#nav" data-toggle="collapse" aria-expanded=false>
                            <span class=sr-only>Toggle navigation</span>
                            <span class=icon-bar></span>
                            <span class=icon-bar></span>
                            <span class=icon-bar></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse " id="nav" >
                        <ul class="nav navbar-nav">
                            <li><a href="/">Trang chủ</a></li>
                            <?php if ($items && !empty($items)): ?>
                                <?php foreach ($items as $n => $item): ?>
                                    <li class=dropdown>
                                        <?= Html::a($item->title . ($item->children ? '<b class="fa fa-angle-down"></b>' : ''), $item->getUrl(), [
                                            'class' => 'dropdown-toggle',
                                             'data-toggle' => 'dropdown'
                                        ]) ?>
                                        <?php if ($item->children): ?>
                                            <div class="dropdown-menu multi-column columns-2 wrap-col-menu <?php if($n >3): ?> edit_potion_sub_menu_1 <?php endif; ?>">
                                                <ul class="multi-column-dropdown">
                                                    <?php foreach ($item->children as $child): ?>
                                                        <li class="col-md-4">
                                                            <?= Html::a($child->title, $child->getUrl()) ?>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <li><a rel=nofollow href="/lien-he">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
<div class="mobile-navbar visible-xs">
    <a href="/"><h3>Trang chủ</h3></a>
    <?php if ($items && !empty($items)) {
        foreach ($items as $item) {
            echo Html::a(Html::tag('h3', $item->title), $item->getUrl());
        }
    } ?>
<!--    --><?//= Html::a(Html::tag('h3', 'Mẫu nhà đẹp'), base_url('mau-nha-dep')) ?>
<!--    --><?//= Html::a(Html::tag('h3', 'Báo giá'), base_url('bao-gia')) ?>
<!--    --><?//= Html::a(Html::tag('h3', 'Kinh nghiệm hay'), route_to('news')) ?>
    <?= Html::a(Html::tag('h3', 'Liên hệ'), base_url('lien-he')) ?>
</div>
