<?php

use App\Helpers\Html;

/**
 * @var \App\Models\ProjectCategoryModel[] $categories
 */
?>
<aside class="panel panel-inverse hidden-xs">
    <div class="panel-heading text-center product-font-size">
        <h3>MÀNG CO</h3>
        <hr class="underline-title">
    </div>
    <div class="panel-body">
        <div class="row">

            <?php if ($category) { ?>
                <?php foreach ($category as $cr_gr) { ?>
                    <div class="col-xs-12 col-md-12">
                        <a href="<?= $cr_gr->getUrl() ?>" class="thumbnail product-box"
                           title="<?= Html::decode($cr_gr->title) ?>">
                            <div class="img-wrap">
                                <?= Html::img($cr_gr->getImage(), ['alt' => $cr_gr->title]) ?>
                            </div>
                            <div class="caption" style="height: 50px">
                                <span><?= Html::decode($cr_gr->title) ?></span>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>
</aside>
