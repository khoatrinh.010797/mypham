<?php

use App\Helpers\Html;

/**
 * @var array $nav
 */
?>
<footer>
    <div class="container footer">
        <div class="col-md-3">
            <div class="logo edit_potion_logo">
                <a href="/" title="Về trang chủ">
                    <img src=<?php echo base_url('/images/' . $settings['home_logo_link']) ?> alt="hoàng phúc"
                    class=img-responsive>
                </a>
            </div>
            <h3 style="font-size: 14px;"><strong><?= $settings['home_ten_doanh_nghiep'] ?></strong></h3>
            <a href="<?= $settings['home_link_facebook'] ?>">
                <img class="img-nav-btn-tel mx-2" src="/images/facebook.png" style="margin-right: 10px">
            </a>
            <a href="<?= $settings['home_zalo'] ?>">
                <img class="img-nav-btn-tel mx-2" src="/images/zalo-min.png" style="margin-right: 10px">
            </a>
            <a href="<?= $settings['home_link_youtube'] ?>">
                <img class="img-nav-btn-tel mx-2" src="/images/Youtube.png" style="margin-right: 10px">
            </a>
            <a href="<?= $settings['home_link_twitter'] ?>">
                <img class="img-nav-btn-tel " src="/images/Ins.png">
            </a>
        </div>
        <div class=col-md-3>
            <h6 class="text_2">SHOWROOM TP.VINH</h6>
            <div style="padding-left: 0px;">
                <p>
                    <span><strong>Mã số thuế: </strong><?= $settings['home_ma_so_thue'] ?><strong> 
                        <p>Ngày đăng ký: </strong><?= $settings['home_ngay_dang_ky_mst'] ?></span></p>
                </p>
                <p>
                <span>
                    <strong>Email: </strong>
                    <span><?= $settings['home_email'] ?></span>
                </span>
                </p>
                <p>
                <span>
                    <strong>Địa chỉ VP: </strong>
                    
                        <a href="<?= $settings['home_link_map_dia_chi'] ?>" target="_blank"
                        ><?= $settings['home_dia_chi'] ?></a>
                    
                    <strong><br>Chi Nhánh:</strong>
                    <span>
                        <a href="<?= $settings['home_link_map_chi_nhanh'] ?>"><?= $settings['home_chi_nhanh'] ?></a>
                    </span>
                </span>
                </p>
            </div>
        </div>
        <div class=col-md-3>
            <h6 class="text_2">HOTLINE</h6>
            <div>
                <p>
                    <strong>
                        <a href="tel:<?= $settings['home_goi_ngay'] ?>" style=color:#ff0000>
                            <img class="img_3 lazy" src="/images/foot_3.png">
                            <?= $settings['home_goi_ngay'] ?></a>
                    </strong>
                </p>
                <p>
                    <strong>
                        <a href="tel:<?= $settings['home_hot_line'] ?> " style=color:#ff0000>
                            <img class="img_3 lazy" src="/images/foot_3.png">
                            <?= $settings['home_hot_line'] ?></a>
                    </strong>
                </p>
            </div>
            <h3 style="font-size: 14px"><strong>THỜI GIAN LÀM VIỆC</strong></h3>
            <p>Thứ 2 - Chủ nhật: 8h00 - 18h00</p>
            <P>Trừ ngày lễ</P>
            <h3 style="font-size: 14px"><strong>TIẾP ĐIỆN THOẠI NGOÀI GIỜ</strong></h3>
            <P>Đến 21h hàng ngày</P>
        </div>
        <div class=col-md-3>
            <h6 class="text_2">Hỗ trợ khách hàng</h6>
            <div style="padding-left: 0px">
                <?php if ($nav): ?>
                    <?php foreach ($nav as $item): ?>
                        <p style="text-align:left">
                            <a href="<?=base_url($item->slug)?>"><?=$item->title?></a>
                        </p>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

        </div>
    </div>
</footer>
<a href=# id=top>↑</a>
<!-- <a href="tel:<?= $settings['home_hot_line'] ?>" id="callButton" class="hidden-xs">
    <img src="/images/hot-line.png">
</a> -->
<ul id=mobile-bar class=visible-xs>

    <li style="background-color:white">
        <a href="tel:<?= $settings['home_goi_ngay'] ?>" target="_blank">
            <img src=/images/za-lo.png width=36>
        </a>
    </li>
    <li style="background-color:white">
        <a href="tel:<?= $settings['home_hot_line'] ?>" target="_blank">
            <img src="/images/hot-line.png?v=1" width=36>
        </a>
    </li>

    <li style="background-color:white">
        <a data-toggle="modal" data-target="#contactModal">
            <img src="/images/lien-he.png" width=36>
        </a>
    </li>
    <li style="background-color:white">
        <a href="<?= $settings['home_link_facebook'] ?>" target=_blank>
            <img src="/images/face-book.png?v1" width=36>
        </a>
    </li>


</ul>

<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="contactFormAjax" action="<?= route_to('home_register') ?>" method="post">
            <div class="modal-content">
                <div class="modal-header hidden">
                    <span>Gửi yêu cầu</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding: 0px">

                    <div class="form-group" style="margin-bottom: 0px">
                        <?= Html::input('text', 'full_name', '', [
                            'placeholder' => 'Họ tên',
                            'required' => true,
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px">
                        <?= Html::input('email', 'email', '', [
                            'placeholder' => 'Địa chỉ email',
                            'required' => false,
                            'class' => 'form-control hidden'
                        ]) ?>

                    </div>
                    <div class="form-group" style="margin-bottom: 0px">
                        <?= Html::input('tel', 'phone', '', [
                            'placeholder' => 'Số điện thoại',
                            'required' => true,
                            'class' => 'form-control'
                        ]) ?>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px">
                        <?= Html::textarea('request', '', [
                            'placeholder' => 'Yêu cầu',
                            'required' => true,
                            'class' => 'form-control',
                        ]) ?>
                    </div>
                    <?= Html::hiddenInput('ref_url', current_url()) ?>
                    <div class="g-recaptcha" data-sitekey="6Lc0hskUAAAAAMmUCydmMLuiApwcbMyQpygWkHei"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button class="btn btn-primary" type="submit">Gửi yêu cầu</button>
                </div>
            </div>
        </form>

    </div>
</div>

<div class="modal fade" id="contactConfirmModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span>Gửi yêu cầu</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <?= Html::label('Gửi yêu cầu thành công') ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>