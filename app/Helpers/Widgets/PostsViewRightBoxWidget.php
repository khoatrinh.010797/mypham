<?php

namespace App\Helpers\Widgets;


use App\Libraries\BaseView;
use App\Models\NewsModel;
use App\Models\PostsModel;
use App\Models\SettingsModel;

class PostsViewRightBoxWidget extends BaseWidget
{

    /**
     * @param BaseView $view
     * @param array $data
     * @return string
     */
    public static function register(BaseView $view, array $data = [])
    {
        $model = (new PostsModel())
            ->where('is_lock', 0)
            ->orderBy('view', 'DESC')
            ->findAll(15);

        return static::render($view, 'posts_view_right_box', [
            'models' => $model
        ]);
    }
}