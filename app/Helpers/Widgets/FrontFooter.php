<?php

namespace App\Helpers\Widgets;


use App\Libraries\BaseView;
use App\Models\ContentModel;
use App\Models\SettingsModel;

class FrontFooter extends BaseWidget
{

    /**
     * @param BaseView $view
     * @param array $data
     * @return string
     */
    public static function register(BaseView $view, array $data = [])
    {
        $nav = (new ContentModel())
            ->where('is_lock',0)
            ->orderBy('updated_at','DESC')
            ->findAll();

        $settings =  new SettingsModel();
        $settings = $settings->findAll();
        $setting_array = [];
        if($settings){
            foreach ($settings as $setting){
                $setting_array[$setting->key] = $setting->value;
            }
        }
        return static::render($view, 'front_footer', [
            'nav' => $nav,
            'settings' => $setting_array
        ]);
    }
}