<?php

namespace App\Helpers\Widgets;


use App\Libraries\BaseView;
use App\Models\NewsModel;
use App\Models\CategoryModel;
use App\Models\SettingsModel;

class NewCategoryNilonRightBoxWidget extends BaseWidget
{

    /**
     * @param BaseView $view
     * @param array $data
     * @return string
     */
    public static function register(BaseView $view, array $data = [])
    {
        $categories = (new CategoryModel())
            ->where('is_lock', 0)
            ->where('parent_id', 82)
            ->orderBy('updated_at', 'DESC')
            ->findAll(5);

        if (!$categories || empty($categories)) return null;
        return static::render($view, 'right_box_new_category_nilon', [
            'category' => $categories
        ]);
    }

}